# trivia-game
### Vue.js app that displays a list of multiple-choice questions (10), where the user seclects one of the possible answers, once the user went through all the questions, the user then is presented with two lists one containing all the quiz questions and the correct answers, and the other is the list of questions and the user's answer. In addition, the score will be displayed on top of the lists (10 points per-correct answer).

### ![screenshot](https://gitlab.com/iamamir/trivia-game/-/raw/master/src/assets/start-game-img.PNG)
### ![screenshot](https://gitlab.com/iamamir/trivia-game/-/raw/master/src/assets/quiz-img.PNG)
### ![screenshot](https://gitlab.com/iamamir/trivia-game/-/raw/master/src/assets/results-img.PNG)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
